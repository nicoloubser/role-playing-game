<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpponentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opponents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20);
            $table->integer('type')->comment('What kind of character is this.');
            $table->string('description', 200)->comment('A quick description about this opponent.');
            $table->integer('avatar')->default(1)->comment('The image linked to this user.');
            $table->integer('fighting')->default(5)->comment('The power of the characters fighting.');
            $table->integer('running')->default(5)->comment('The power of the characters running.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opponents');
    }
}
