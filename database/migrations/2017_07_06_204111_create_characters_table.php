<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id');
            $table->string('name', 20);
            $table->integer('type')->comment('What kind of character is this.');
            $table->integer('avatar')->default(1)->comment('The image linked to this user.');
            $table->integer('fighting')->default(5)->comment('The power of the characters fighting.');
            $table->integer('running')->default(5)->comment('The power of the characters running.');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}
