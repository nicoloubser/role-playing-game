<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Relational table between places and objects. ONe place can have many objects, many objects can belong to on or many places
 *
 */
class CreateObjectsPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objects_places', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('places_id');
            $table->unsignedInteger('objects_id');
            $table->timestamps();
            $table->foreign('places_id')->references('id')->on('places');
            $table->foreign('objects_id')->references('id')->on('objects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('object_place');
    }
}
