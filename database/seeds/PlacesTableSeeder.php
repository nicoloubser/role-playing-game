<?php

use Illuminate\Database\Seeder;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id'=>1, 'name'=>'graveyard', 'type'=>'1', 'avatar'=>'1'],
            ['id'=>2, 'name'=>'forest', 'type'=>'2', 'avatar'=>'2'],
            ['id'=>3, 'name'=>'town', 'type'=>'3', 'avatar'=>'3'],
            ['id'=>4, 'name'=>'house', 'type'=>'4', 'avatar'=>'4']
        ];

        $place = app(\App\Repositories\Eloquent\Place::class);
        
        foreach ($data as $value) {
            $place->create($value);
        }
    }
}
