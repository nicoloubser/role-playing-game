<?php

use Illuminate\Database\Seeder;

class ObjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id'=>1, 'name'=>'trees', 'type'=>'1', 'avatar'=>'1'],
            ['id'=>2, 'name'=>'stones', 'type'=>'2', 'avatar'=>'2'],
            ['id'=>3, 'name'=>'table', 'type'=>'3', 'avatar'=>'3'],
            ['id'=>4, 'name'=>'chair', 'type'=>'4', 'avatar'=>'4'],
            ['id'=>5, 'name'=>'tombstone', 'type'=>'5', 'avatar'=>'5'],
            ['id'=>6, 'name'=>'grave', 'type'=>'6', 'avatar'=>'6'],
            ['id'=>7, 'name'=>'flowers', 'type'=>'7', 'avatar'=>'7'],
            ['id'=>8, 'name'=>'houses', 'type'=>'8', 'avatar'=>'8'],
            ['id'=>9, 'name'=>'church', 'type'=>'9', 'avatar'=>'9'],
            ['id'=>10, 'name'=>'carriage', 'type'=>'10', 'avatar'=>'10'],
            ['id'=>11, 'name'=>'grocer', 'type'=>'11', 'avatar'=>'11'],
            ['id'=>12, 'name'=>'chains', 'type'=>'12', 'avatar'=>'12'],
            ['id'=>13, 'name'=>'river', 'type'=>'13', 'avatar'=>'13'],
            ['id'=>14, 'name'=>'window', 'type'=>'13', 'avatar'=>'14'],
            ['id'=>15, 'name'=>'bed', 'type'=>'13', 'avatar'=>'15']
        ];
        $object = app(\App\Repositories\Eloquent\Object::class);
        foreach ($data as $value) {
            $object->create($value);
        }
    }
}
