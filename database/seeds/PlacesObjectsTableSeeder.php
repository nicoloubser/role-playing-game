<?php

use Illuminate\Database\Seeder;

class PlacesObjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['places_id'=>1, 'objects_id'=>2],
            ['places_id'=>1, 'objects_id'=>5],
            ['places_id'=>1, 'objects_id'=>6],
            ['places_id'=>1, 'objects_id'=>7],
            ['places_id'=>1, 'objects_id'=>12],
            ['places_id'=>2, 'objects_id'=>1],
            ['places_id'=>2, 'objects_id'=>2],
            ['places_id'=>2, 'objects_id'=>7],
            ['places_id'=>2, 'objects_id'=>13],
            ['places_id'=>3, 'objects_id'=>1],
            ['places_id'=>3, 'objects_id'=>2],
            ['places_id'=>3, 'objects_id'=>8],
            ['places_id'=>3, 'objects_id'=>9],
            ['places_id'=>3, 'objects_id'=>10],
            ['places_id'=>3, 'objects_id'=>11],
            ['places_id'=>4, 'objects_id'=>3],
            ['places_id'=>4, 'objects_id'=>4],
            ['places_id'=>4, 'objects_id'=>7],
            ['places_id'=>4, 'objects_id'=>14],
            ['places_id'=>4, 'objects_id'=>15],
        ];

        $objectPlace = app(\App\Repositories\Eloquent\ObjectPlace::class);
        
        foreach ($data as $value) {
            $objectPlace->create($value);
        }

    }
}
