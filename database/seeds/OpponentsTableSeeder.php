<?php

use Illuminate\Database\Seeder;

class OpponentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id'=>1, 'name'=>'Dark-lord', 'description'=>'Evil shadow creature', 'type'=>'1', 'avatar'=>'1'],
            ['id'=>2, 'name'=>'Night-crawler', 'description'=>'Witless. Dangerous in dark alleys', 'type'=>'2', 'avatar'=>'2'],
            ['id'=>3, 'name'=>'Day-eater', 'description'=>'Sucks your soul dry', 'type'=>'3', 'avatar'=>'3'],
            ['id'=>4, 'name'=>'Zombie', 'description'=>'Lousy at everything except killing', 'type'=>'4', 'avatar'=>'4'],
            ['id'=>5, 'name'=>'Witch', 'description'=>'Deals in herbs, magic spells and death', 'type'=>'3', 'avatar'=>'3'],
            ['id'=>6, 'name'=>'Wizard', 'description'=>'Likes to do two things, as long as both of those things are killing', 'type'=>'4', 'avatar'=>'4']
        ];

        $opponent = app(\App\Repositories\Eloquent\Opponent::class);
        
        foreach ($data as $value) {
            $opponent->create($value);
        }
    }
}
