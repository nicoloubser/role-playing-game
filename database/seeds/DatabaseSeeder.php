<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ObjectsTableSeeder');
        $this->call('PlacesTableSeeder');
        $this->call('PlacesObjectsTableSeeder');
        $this->call('OpponentsTableSeeder');
    }
}
