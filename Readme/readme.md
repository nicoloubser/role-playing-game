Role playing game

With this game, you can do the following

* You can create a character
* You can explore a location
* You can move to a new location
* You will encounter a villian in a new location, where you can either fight, or run. Your fighting or running skills will be affected
by a loss or a win
* You can save a game state. This basically means that you fighting and running, which is session based, will saved to you characters profile
in the DB.

There are also screenshot depcting the results as interpreted by the CLI tool. playgame_presave and playgame_after_save


Directions on using the API
------ API ------

The API will always govern what can happen next, and what is happening. The API sends the next available url that can be accessed in it's payload to the client.
This will let the system know where to post its next action to. The API is always in control.

******************Creating your character******************
Step one : Create a character
     curl -X POST http://your.url/character -d 'type=1&name=character_name&avatar=3&fighting=5&running=5'
     Not all values you are posting are currently being used. Some are for version 2.
        Version 2
            avatar to be displayed
            type to be enforced

******************To play via API******************

    YOu need to call the start route first, and once. After you called this, you can call whatever the response returned under "next_page"(should next-move, see "Choosing explore" below)
    This will also create session data linked to your character id

    curl http://your.url/start/character_id

    Example return
    {
      "messages": {
        "greeting": "Hello. You are in the *NAME OF LOCATION*.",
        "question": "Which direction would you like to go? "
      },
      "actions": {
        "user_act_on": 1,
        "possible_user_actions": [
          "left (0)",       <- WILL RESULT IN BATTLE
          "forward (1)",    <- WILL RESULT IN BATTLE
          "right (2)",      <- WILL RESULT IN BATTLE
          "explore (3)",    <- WILL RESULT IN EXPLORATION
          "save game (4)"
        ]
      },
      "battle_results": null,
      "location": [
        {
          "id": 3,
          "name": "town",
          "avatar": 3
        }
      ],
      "opponent": null,
      "next_page": "next-move",     <- THE NEXT ROUTE THAT WILL BE CALLED
      "character": {
        "id": 1,
        "player_id": 0,
        "name": "my_character",
        "type": 1,
        "avatar": 3,
        "fighting": 5,
        "running": 5,
        ....
      }
    }

******************Choosing explore******************
This allows you to explore your current location
curl http://your.url/next-move/character_id/ACTION (Action = 3)

Example output
{
  "messages": {
    "greeting": "You are exploring and seeing the following object : *OBJECT SEEN DURING EXPLORATION*",
    "question": "Which direction would you like to go? "
  },
  "actions": {
    "user_act_on": 1,
    "possible_user_actions": [
        "left (0)",       <- WILL RESULT IN BATTLE
        "forward (1)",    <- WILL RESULT IN BATTLE
        "right (2)",      <- WILL RESULT IN BATTLE
        "explore (3)",    <- WILL RESULT IN EXPLORATION
        "save game (4)"
    ]
  },
  "battle_results": null,
  "location": {
    "id": 3,
    "name": "town",
    "type": 3,
    "avatar": 3,
    "created_at": "2017-07-08 17:34:56",
    "updated_at": "2017-07-08 17:34:56",
    "objects": [
      {
        "id": 1,
        "name": "trees",
        "type": 1,
        "avatar": 1,
        "created_at": "2017-07-08 17:34:56",
        "updated_at": "2017-07-08 17:34:56",
        "pivot": {
          "places_id": 3,
          "objects_id": 1
        }
      },
      ....ommitted....
  },
  "opponent": null,
  "next_page": "next-move",
  "character": {
    "id": 1,
    "player_id": 0,
    "name": "onacwqeqwefff",
    "type": 1,
    "avatar": 3,
    "fighting": 5, <- YOUR CURRENT FIGHTING POWER
    "running": 5,  <- YOUR CURRENT RUNNING POWER
    "created_at": "2017-07-08 17:36:56",
    "updated_at": "2017-07-09 12:45:43"
  }
}

******************Choosing a new location, and battle******************
When you go to a new place(0,1,2), you will always run into an adversary
curl http://your.url/next-move/character_id/ACTION (0,1,2,3,4)

Example output
{
  "messages": {
    "greeting": "You are walking to a new location and came upon the following : *NEW LOCATION*.",
    "question": "You encountered a villian! Do you run(2) or do you FIGHT(1) "  <- HOW WILL YOU REACT TO THE VILLIAN?
  },
  "actions": {
    "user_act_on": 1,
    "possible_user_actions": [
      "fight (2)",
      "run (1)"
    ]
  },
  "battle_results": null,
  "location": [
    {
      "id": 2,
      "name": "forest",
      "avatar": 2
    }
  ],
  "opponent": {             <-DESCRIPTION OF THE VILLIAN
    "id": 3,
    "name": "Day-eater",
    "description": "Sucks your soul dry",
    "avatar": 3,
    "fighting": 5, <- YOUR CURRENT FIGHTING POWER
    "running": 5,  <- YOUR CURRENT RUNNING POWER
  },
  "next_page": "battle",    <- THE NEXT PAGE IS THE BATTLE PAGE, YOU CAN RUN OR FIGHT
  "character": {
    "id": 1,
    "player_id": 0,
    "name": "MY_CHARACTER",
    "type": 1,
    "avatar": 3,
    "fighting": 5,
    "running": 5,
    "created_at": "2017-07-08 17:36:56",
    "updated_at": "2017-07-09 12:45:43"
  }
}

Example reaction to a fight
{
  "messages": {
    "greeting": "Your battle is done. Did you win? NO", <-CONTAINS A WRITTEN RESULT OF THE BATTLE
    "question": "Which direction would you like to go? "
  },
  "actions": {
    "user_act_on": 1,
    "possible_user_actions": [
      "left (0)",
      "forward (1)",
      "right (2)",
      "explore (3)",
      "save game (4)"
    ]
  },
  "battle_results": {       <-- ACTUAL BATTLE RESULTS, CAN BE USED BY THE INTEGRATING CLIENT
    "player_wins": false,
    "add_to_score": -1
  },
  "location": {
    "id": 3,
    "name": "town",
    "type": 3,
    "avatar": 3,
     ....ommitted
  },
  "opponent": null,
  "next_page": "next-move", <-- NEXT PAGE A CLIENT WILL BE CALLING
  "character": {
   ....
    "avatar": 3,
    "fighting": 4, <- YOU HAVE LOST, SO YOUR FIGHTING SKILLS DIMINISHED
    "running": 5,
...
  }
}

******************SAVING A GAME******************
To save a game, just send a '4'

- Command line implementation
Just run

php artisan playgame

ANd follow the prompts