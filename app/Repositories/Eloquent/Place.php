<?php
namespace App\Repositories\Eloquent;

class Place extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'App\Models\Places';
    }

    public function includeRelations(int $id, array $columns = ['*'], array $relations = [])
    {
        $relationships = implode(',', $relations);
        return $this->model->with()->find($id, $columns);
    }
}