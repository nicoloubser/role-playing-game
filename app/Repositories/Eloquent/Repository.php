<?php namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\IRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

/**
 * Class Repository
 * @package Bosnadev\Repositories\Eloquent
 */
abstract class Repository implements IRepository
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var
     */
    protected $model;

    protected $withRelationships = '';

    /**
     * @param App $app
     * @throws \App\Exceptions\RepositoryException
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract function model();

    /**
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all($columns = ['*']) : \Illuminate\Database\Eloquent\Collection
    {
        $this->newQuery()->eagerLoadRelations();
        return $this->model->get($columns);
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) : \Illuminate\Database\Eloquent\Model 
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return int
     */
    public function update(array $data, $id, $attribute="id") : int
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * @param $id
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id, $columns = ['*']) : \Illuminate\Database\Eloquent\Model
    {
        $this->newQuery()->eagerLoadRelations();
        return $this->model->find($id, $columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findBy($attribute, $value, $columns = ['*']) : \Illuminate\Database\Eloquent\Model
    {
        $this->newQuery()->eagerLoadRelations();
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws RepositoryException
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model->newQuery();
    }

    /**
     * with
     *
     * @param $relations
     * @return $this
     */
    public function with($relations)
    {
        if (is_string($relations))
        {
            $relations = func_get_args();
        }
        $this->withRelationships = $relations;
        return $this;
    }

    /**
     * eagerLoadRelations
     *
     * @return $this
     */
    protected function eagerLoadRelations()
    {
        if(!empty($this->withRelationships))
        {
            foreach ($this->withRelationships as $relation)
            {
                $this->model->with($relation);
            }
        }
        return $this;
    }

    /**
     * newQuery
     *
     * @return $this
     */
    public function newQuery()
    {
        $this->model = $this->model->newQuery();
        return $this;
    }

    /**
     * count
     *
     * @return integer
     */
    public function count()
    {
        return $this->all(['*'])->count();
    }
}