<?php
namespace App\Repositories\Eloquent;

class ObjectPlace extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\ObjectPlace';
    }
}