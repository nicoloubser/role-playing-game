<?php
namespace App\Repositories\Eloquent;

class Object extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'App\Models\Objects';
    }
}