<?php
namespace App\Repositories\Eloquent;

class Opponent extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Opponents';
    }
}