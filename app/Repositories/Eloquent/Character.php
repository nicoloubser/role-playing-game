<?php
namespace App\Repositories\Eloquent;

class Character extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Models\Characters';
    }
}