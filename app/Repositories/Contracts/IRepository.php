<?php
namespace App\Repositories\Contracts;
/**
 * Keeps the promises for the repository mappings
 */
interface IRepository
{
    public function all($columns = ['*']) : \Illuminate\Database\Eloquent\Collection;

    public function create(array $data) : \Illuminate\Database\Eloquent\Model;

    public function update(array $data, $id) : int;

    public function find($id, $columns = ['*']) : \Illuminate\Database\Eloquent\Model;

    public function findBy($field, $value, $columns = ['*']) : \Illuminate\Database\Eloquent\Model;
}