<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('Opponent', function () {
            return app(\App\Explorer\Opponents::class);
        });

        App::bind('SaveGame', function () {
            return app(\App\Explorer\SaveGame::class);
        });
    }
}
