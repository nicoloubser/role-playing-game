<?php namespace App\Providers;

use App\Models\CreditCards\CreditCardEntity;
use App\Lib\Owner\Owner;
use App\Console\Commands\KeyManagement;
use App\Console\Commands\Verify;
use App\Console\Commands\Get;
use App\Console\Commands\RemoveOldCards;
use App\Console\Commands\ReloadDek;
use App\Console\Commands\DekSanity;
use App\Console\Commands\KekSanity;
use App\Console\Commands\SetupUsers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use App;
use App\Console\Commands\DecryptVerifier;

class CommandServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton('command.setup.playgame', function () {
            return new App\Console\Commands\PlayGame();
        });

        $this->commands(
            'command.setup.playgame'
        );

    }
}
