<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Characters extends Model
{
    protected $fillable = ['player_id', 'name', 'type', 'avatar', 'fighting', 'running'];
}