<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Places extends Model
{
    protected $fillable = ['name', 'type', 'avatar'];

    public function objects()
    {
        return $this->belongsToMany('App\Models\Objects');
    }
}