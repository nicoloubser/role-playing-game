<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * PIvot table between object and place
 */
class ObjectPlace extends Model
{
    protected $table = 'objects_places';
    protected $fillable = ['places_id', 'objects_id'];
}