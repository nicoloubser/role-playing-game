<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Opponents extends Model
{
    protected $fillable = ['name', 'type', 'avatar', 'fighting', 'running'];
}