<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Objects extends Model
{
    protected $fillable = ['name', 'type', 'avatar'];

    public function places()
    {
        return $this->hasMany('App\Models\Places');
    }
}