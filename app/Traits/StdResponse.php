<?php

namespace App\Traits;

/**
 * Trait since it may be used in difference concerns
 */
trait StdResponse
{
    /**
     * Admin tasks formatter
     *
     * @param $response
     * @param int $code
     * @param string $message
     * @return mixed
     * 
     */
    public function response($response, $code = 200, $message = '')
    {
        $return = [
            $response
        ];

        return response()->json($return, $code);
    }
}