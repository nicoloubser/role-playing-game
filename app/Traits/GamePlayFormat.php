<?php
/**
 * Created by PhpStorm.
 * User: nico
 * Date: 2017/07/09
 * Time: 12:09 AM
 */

namespace App\Traits;

/**
 * Handles the format that facilitates the interactions between the player and the game engine
 */
trait GamePlayFormat
{
    // The type of routes that can be requested up user action
    protected $nextPageIsNextMove = 'next-move';
    protected $nextPageIsBattle = 'battle';

    protected $greeting = null;
    protected $question = null;
    // Is the user acton on position change or interaction
    protected $actOn = null;
    // THe possible things to do based on actOn
    protected $possibleActions = null;
    protected $generatedLocation = null;
    protected $opponent;

    protected $battleResults = null;
    protected $nextPageToCall = null;
    
    /**
     * Creates the structure for the recieveing end to act upon.
     *
     * @return array
     */
    public function format() : array
    {
        $body = [
            'messages' => [
                'greeting' => $this->greeting,
                'question' => $this->question
            ],
            'actions' => [
                'user_act_on' => $this->actOn,
                'possible_user_actions' => $this->possibleActions,
            ],
            'battle_results' => $this->battleResults,
            'location' => $this->generatedLocation,
            'opponent' => $this->opponent,
            'next_page' => $this->nextPageToCall,
            'character' => null
        ];
        return $body;
    }
}