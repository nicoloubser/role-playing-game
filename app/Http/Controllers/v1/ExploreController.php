<?php

namespace App\Http\Controllers\v1;

use App\Explorer\Characters;
use App\Explorer\Explore;
use App\Explorer\Skirmish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class ExploreController extends Controller
{
    private $explore;
    private $request; 
    
    /**
     * ExampleController constructor.
     *
     * @param Explore $explore
     */
    public function __construct(Explore $explore, Characters $character, Request $request)
    {
        $this->character = $character;
        $this->explore = $explore;
        $this->request = $request;
    }

    /**
     * start
     *
     * @param int $characterId
     *
     * @return array
     */
    public function start(int $characterId = 0)
    {
        $character = $this->character->find($characterId);
        $initialData = $this->explore->initialise();
        
        // Save the state of the game in volatile memory
        Cache::put($characterId.'_character', $character, 100);
        Cache::put($characterId.'_location', $initialData['location'][0], 100);
        
        $initialData['character'] = $character;
        
        return $initialData;
    }

    /**
     * nextMove
     *
     * @param int $characterId
     * @param int $action
     *
     * @return array
     */
    public function nextMove(int $characterId = 0, int $action)
    {
        // What has been chose? left, right, forward, explore?
        switch ($action) {
            // Position change
            case 0 :
            case 1 :
            case 2 :
                $opponents = App::make('Opponent');
                $nextMove = $this->explore->moveOn($opponents);
                // Persist the opponent
                
                Cache::put($characterId.'_opponent', $nextMove['opponent'], 100);
                $character = json_decode(Cache::get($characterId.'_character'));
                
                $nextMove['character'] = $character;
                return $nextMove;
                break;
            // Explore
            case 3 :
                $explore = $this->explore->explore(Cache::get($characterId.'_location')['id']);
                $character = json_decode(Cache::get($characterId.'_character'));
                $explore['character'] = $character;
                return $explore;
                break;
            case 4 :
                /**
                 * @var $saveGame \App\Explorer\SaveGame
                 */
                $saveGame = App::make('SaveGame');
                return $saveGame->save($this->character, $characterId);
            default :
                break;
        }
    }

    /**
     * battle
     *
     * @param Skirmish $skirmish
     * @param int $characterId
     * @param int $action
     *
     * @return array
     */
    public function battle(Skirmish $skirmish, int $characterId = 0, int $action)
    {
        $character = Cache::get($characterId.'_character');
        $opponent = Cache::get($characterId.'_opponent');
        $location = Cache::get($characterId.'_location');

        $character = json_decode($character, true);

        switch ($action) {
            case 1 :
                $result = $this->explore->run($skirmish, $location['id'], $character['running'], $opponent['running']);
                $character['running'] += $result['battle_results']['add_to_score'];
                break;
            case 2 :
                $result = $this->explore->fight($skirmish, $location['id'], $character['fighting'], $opponent['fighting']);
                $character['fighting'] += $result['battle_results']['add_to_score'];
                break;
            default :
                return "That is not a valid action";
        }
        Cache::put($characterId.'_character', json_encode($character), 100);
        $result['character'] = $character;
        return $result; 
    }
}
