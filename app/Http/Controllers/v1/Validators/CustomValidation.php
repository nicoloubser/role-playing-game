<?php

namespace App\Http\Controllers\v1\Validators;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomValidation
{
    /**
     * isValid
     *
     * @param Request $request
     *
     * @return mixed bool on success, array of errors on failure
     *
     */
    public function isValid(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules);
        return $validator->fails() ? $validator->errors() : true;
    }
}