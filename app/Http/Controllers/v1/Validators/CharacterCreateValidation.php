<?php
namespace App\Http\Controllers\v1\Validators;

class CharacterCreateValidation extends CustomValidation
{
    protected $rules = [
        'name' => 'unique:mysql.characters,name|required|string|max:20',
        'player_id' => 'exists:players,id',
        'type' => 'required|integer',
        'avatar' => 'required|integer',
        'fighting' => 'required|integer',
        'running' => 'required|integer'
    ];    
}