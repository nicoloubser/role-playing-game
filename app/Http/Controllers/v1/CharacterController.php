<?php

namespace App\Http\Controllers\v1;

use App\Repositories\Eloquent\Character;
use App\Http\Controllers\v1\Validators\CharacterCreateValidation;
use App\Repositories\Eloquent\Place;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CharacterController extends Controller
{
    protected $character;
    protected $request;
    
    /**
     * Create a new controller instance.
     */
    public function __construct(Character $character, Request $request)
    {
        $this->character = $character;
        $this->request = $request;
    }

    /**
     * create
     *
     * @param CharacterCreateValidation $validation
     * @return mixed
     */
    public function create(CharacterCreateValidation $validation)
    {
        // Input validation of POST data
        if (($result = $validation->isValid($this->request)) !== true) {
            return $this->response(['result' => $result], Response::HTTP_BAD_REQUEST);
        }

        $response = $this->formatResponse($this->character->create($this->request->all()));
        return $this->response($response->getOriginalContent(), $response->getStatusCode());
    }

    /**
     * Fetches a single record
     *
     * @param $id
     * @return mixed
     */
    public function fetch($id)
    {
        $response = $this->formatResponse($this->character->find($id));
        return $this->response($response->getOriginalContent(), $response->getStatusCode());
    }

    /**
     * Fetches all the characters
     *
     * @return mixed
     */
    public function fetchAll()
    {
        $response = $this->formatResponse($this->character->all());
        return $this->response($response->getOriginalContent(), $response->getStatusCode());
    }
}
