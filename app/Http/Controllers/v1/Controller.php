<?php

namespace App\Http\Controllers\v1;

use App\Traits\StdResponse;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use StdResponse;

    /**
     * This is admin tasks, not gameplay
     *
     * @param $result
     * @return array
     */
    protected function formatResponse($result)
    {
        if ($result === false) {
            $functionResponse = response(['result' => 'failed'], Response::HTTP_BAD_REQUEST);
        } elseif (empty($result)) {
            $functionResponse = response(['result' => $result], Response::HTTP_NO_CONTENT);
        } else {
            $functionResponse = response(['result' => $result], Response::HTTP_OK);
        }
        return $functionResponse;
    }

}
