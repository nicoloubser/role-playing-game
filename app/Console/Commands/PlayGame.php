<?php namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use App;
use Illuminate\Http\Response;

/**
 * This is a demonstration on how the api can be integrated.
 * The character playing  will always be == 1 as this is to show how the responses can be dealt with.
 */

class PlayGame extends Command
{


    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'playgame';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Prototype. Demonstrates how to integrate the game.";

    protected $signature = 'playgame';

    public function fire()
    {
        // First step, initialise the game
        $characterName = '';
        $characterFighting = '';
        $characterRunning = '';
        $greeting = '';
        $question = '';
        $actions = '';
        $nextPage = '';
        
        $client = app(Client::class);
        $res = $client->request('GET', env('URL').'/start/1');

        // This is the initial step of the game
        if($res->getStatusCode() == Response::HTTP_OK) {
            $returnedData = json_decode($res->getBody(), true);
            [$characterName, $characterFighting, $characterRunning, $greeting, $question, $actions, $nextPage] = $this->preparation($returnedData);
            $this->topOutput($characterName, $characterFighting, $characterRunning, $greeting, $question);
            $answer = $this->action($actions);
            $res = $client->request('GET', env('URL') . '/' . $nextPage . '/1/' . $answer);
        }

        // Now loop through all subsequent steps. This can be done to infinity.
        do {
            if($res->getStatusCode() == Response::HTTP_OK) {
                $returnedData = json_decode($res->getBody(), true);
                [$characterName, $characterFighting, $characterRunning, $greeting, $question, $actions, $nextPage] = $this->preparation($returnedData);
                $this->topOutput($characterName, $characterFighting, $characterRunning, $greeting, $question);
                $this->opponentDetails($returnedData);
                $actionSelected = $this->action($actions);

                switch($actionSelected) {
                    case 4 :
                        $type = 'patch';
                        break;
                    default :
                        $type = 'get';

                }

                $res = $client->$type(env('URL') . '/'. $nextPage . '/1/' . $actionSelected);
            }
        } while ($actionSelected != 4);
        
    }

    /**
     * preparation
     *
     * @param $returnedData
     * @return array
     *
     */
    public function preparation($returnedData)
    {
        $characterName = $returnedData['character']['name'];
        $characterFighting = $returnedData['character']['fighting'];
        $characterRunning = $returnedData['character']['running'];
        $greeting = $returnedData['messages']['greeting'];
        $question = $returnedData['messages']['question'];
        $actions = $returnedData['actions']['possible_user_actions'];
        $nextPage = $returnedData['next_page'];
        return array($characterName, $characterFighting, $characterRunning, $greeting, $question, $actions, $nextPage);
    }

    /**
     * topOutput
     *
     *
     * @param $characterName
     * @param $characterFighting
     * @param $characterRunning
     * @param $greeting
     * @param $question
     *
     */
    public function topOutput($characterName, $characterFighting, $characterRunning, $greeting, $question)
    {
        $this->output->writeln('Player ' . $characterName);
        $this->output->writeln('Fighting ' . $characterFighting . " | Running " . $characterRunning);
        $this->output->writeln($greeting);
        $this->output->writeln($question);
    }

    /**
     * opponentDetails
     *
     * @param $returnedData
     *
     */
    public function opponentDetails($returnedData)
    {
        if (!empty($returnedData['opponent'])) {
            $this->output->writeln('Your opponent is a ' . $returnedData['opponent']['name']);
            $this->output->writeln('Your opponent\'s description is "' . $returnedData['opponent']['description'] . '"');
        }
    }

    /**
     * action
     * @param $actions
     * @return mixed
     *
     */
    public function action($actions)
    {
        $this->output->writeln($actions);
        $answer = strtolower($this->ask('Please select an action'));
        return $answer;
    }

}