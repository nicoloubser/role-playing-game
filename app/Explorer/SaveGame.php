<?php
namespace App\Explorer;

use Illuminate\Support\Facades\Cache;

class SaveGame
{
    /**
     * Saves a game. At this stage we will only see the characters fighting and running powers affected.
     *
     * @param Characters $character
     * @param int $characterId
     *
     * @return mixed
     */
    public function save(Characters $character, int $characterId)
    {
        $temporaryCharacter = json_decode(Cache::get($characterId.'_character'), true);
        return $character->update(
            [
                'fighting'=>$temporaryCharacter['fighting'],
                'running'=>$temporaryCharacter['running']
            ],
            $characterId
        );
    }
}