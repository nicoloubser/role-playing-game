<?php
namespace App\Explorer;
use App\Repositories\Eloquent\Place;

class Locations
{
    private $place;
    
    public function __construct(Place $place)
    {
        $this->place = $place;    
    }

    /**
     * Generates a location
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function generateLocation()
    {
        $results = $this->place->all(['id','name','avatar']);
        $return = $results->where('id', rand(1, $results->count()));
        return $return;
    }

    /**
     * Explores a location
     *
     * @param int $locationId
     *
     * @return \Illuminate\Database\Eloquent\Collection
     *
     */
    public function exploreLocation(int $locationId)
    {
        $return = $this->place->with('objects')->findBy('id', $locationId);
        return $return;
    }
}