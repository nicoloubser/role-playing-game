<?php

namespace App\Explorer;

use App\Traits\GamePlayFormat;

class Explore
{
    use GamePlayFormat;
    
    const GREETING = 'Hello. You are in the '; // Location gets appended
    const EXPLORE = 'You are exploring and seeing the following object : ';
    const MOVE_ON = 'You are walking to a new location and came upon the following : ';
    const QUESTION_MOVEMENT = 'Which direction would you like to go? '; //Random generated direction appended
    const QUESTION_INTERACTION = 'You encountered a villian! Do you run(2) or do you FIGHT(1) ';

    const INFORMATION_BATTLE = 'Your battle is done. Did you win? ';
    const INFORMATION_RUN = 'You fled! Could you outrun you opponent? ';
    
    const DIRECTIONS = [
        0 => 'left (0)',
        1 => 'forward (1)',
        2 => 'right (2)',
        3 => 'explore (3)',
        4 => 'save game (4)'
    ];
    const MOVEMENT = 1;

    const INTERACTIONS = [
        0 => 'fight (2)',
        1 => 'run (1)',
        
    ];
    const ACTION = 2;

    private $location;

    /**
     * Explore constructor.
     * 
     * @param Locations $location
     */
    public function __construct(Locations $location)
    {
        $this->location = $location;
    }

    public function getLocation()
    {
        return $this->location;
    }

    /**
     * All interactions start with this function.
     *
     * @return array
     */
    public function initialise() : array
    {
        // Generate a location
        $this->generatedLocation = array_values($this->location->generateLocation()->jsonSerialize());

        $this->greeting = self::GREETING . $this->generatedLocation[0]['name'] .'.';
        $this->question = self::QUESTION_MOVEMENT;
        $this->actOn = self::MOVEMENT;
        $this->possibleActions = self::DIRECTIONS;
        $this->nextPageToCall = $this->nextPageIsNextMove;
        
        $body = $this->format();

        return $body;
    }

    /**
     * Dictates moving around. Specifies battle as next action
     *
     * @param Opponents $opponents
     *
     * @return array
     */
    public function moveOn(Opponents $opponents) : array
    {
        $this->generatedLocation = array_values($this->location->generateLocation()->jsonSerialize());
        $this->greeting = self::MOVE_ON . $this->generatedLocation[0]['name'] .'.';
        $this->question = self::QUESTION_INTERACTION;
        $this->actOn = self::MOVEMENT;
        $this->possibleActions = self::INTERACTIONS;
        $this->nextPageToCall = $this->nextPageIsBattle;
        $this->opponent = $opponents->generateOpponent();
        $body = $this->format();
        return $body;
    }

    /**
     *  Dictates exploring. Specifies movement/exploring as next action
     *
     * @param int $location
     *
     * @return array
     */
    public function explore(int $location) : array
    {
        $this->greeting = self::EXPLORE . $this->observingLocation($location);
        $this->actOn = self::MOVEMENT;
        $this->question = self::QUESTION_MOVEMENT;
        $this->possibleActions = self::DIRECTIONS;
        $this->generatedLocation = $this->location->exploreLocation($location)->jsonSerialize();
        $this->nextPageToCall = $this->nextPageIsNextMove;
        $body = $this->format();
        return $body;
    }

    /**
     * Returns the name of the observed location
     *
     * @param int $locationId
     * @return string
     */
    public function observingLocation(int $locationId) : string
    {
        $objectIsee = json_decode($this->location->exploreLocation($locationId), true);
        $objectIsee  = $objectIsee['objects'][rand(0, count($objectIsee['objects'])-1)]['name'];
        return $objectIsee;
    }

    /**
     * run
     *
     * @param Skirmish $skirmish
     * @param int $location
     * @param int $playerRun
     * @param int $opponentRun
     * 
     * @return array
     */
    public function run(Skirmish $skirmish, int $location, int $playerRun, int $opponentRun) : array
    {
        $result = $skirmish->battleDeterminer($playerRun, $opponentRun);

        $this->greeting = self::INFORMATION_RUN . ($result['player_wins'] ? 'YES' : 'NO');
        $body = $this->generalVariablePopulation($location, $result);
        return $body;
    }

    /**
     * fight
     *
     * @param Skirmish $skirmish
     * @param int $playerFight
     * @param int $opponentFight
     *
     * @return array
     */
    public function fight(Skirmish $skirmish, int $location, int $playerFight, int $opponentFight) : array
    {
        $result = $skirmish->battleDeterminer($playerFight, $opponentFight);
        
        $this->greeting = self::INFORMATION_BATTLE . ($result['player_wins'] ? 'YES' : 'NO');
        $body = $this->generalVariablePopulation($location, $result);
        return $body;
    }

    /**
     * Due to the simple nature of this game, these actions are accidentally the same, this function should probably
     * be ditched soon
     *
     * @param int $location
     * @param $result
     * @return array
     *
     */
    private function generalVariablePopulation(int $location, $result) : array
    {
        $this->actOn = self::MOVEMENT;
        $this->question = self::QUESTION_MOVEMENT;
        $this->possibleActions = self::DIRECTIONS;
        $this->generatedLocation = $this->location->exploreLocation($location)->jsonSerialize();
        $this->nextPageToCall = $this->nextPageIsNextMove;
        $this->battleResults = $result;
        $body = $this->format();
        return $body;
    }
}