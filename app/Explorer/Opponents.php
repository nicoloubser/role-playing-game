<?php

namespace App\Explorer;

use App\Repositories\Eloquent\Opponent;

class Opponents
{
    private $opponent;
    
    public function __construct(Opponent $opponent)
    {
        $this->opponent = $opponent;
    }

    /**
     * generateOpponent
     *
     * @return \App\Models\Opponents
     */
    public function generateOpponent() : \App\Models\Opponents
    {
        $results = $this->opponent->all(['id', 'name', 'description', 'avatar', 'fighting', 'running']);
        $return = $results->where('id', rand(1, $results->count()))->first();
        return $return;
    }
}