<?php
namespace App\Explorer;

class Skirmish
{
    /**
     * Determines who wins the battle. THere is a '35%' threshold where the most powerfull can still lose
     *
     * @param int $playerRun
     * @param int $opponentRun
     *
     * @return array
     */
    public function battleDeterminer(int $playerRun, int $opponentRun) : array
    {
        $result = [];
        $result['player_wins'] = false;
        $result['add_to_score'] = -1;
        
        switch ($playerRun <=> $opponentRun)
        {
            case -1 :
                $playerWins = rand(0, 100);
                if ($playerWins >= 65) {
                    $result['player_wins'] = true;
                    $result['add_to_score'] = 1;
                }
                break;
            case 0 :
                $playerWins = rand(0, 1);
                if ($playerWins) {
                    $result['player_wins'] = true;
                    $result['add_to_score'] = 1;
                }
                break;
            case 1 :
                $playerWins = rand(0, 100);
                if ($playerWins <= 65) {
                    $result['player_wins'] = true;
                    $result['add_to_score'] = 1;
                }
                break;
            default :
                $result['player_wins'] = false;
                $result['add_to_score'] = -1;
        }
        return $result;
    }
}