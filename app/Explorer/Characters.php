<?php
namespace App\Explorer;

use App\Repositories\Eloquent\Character;

class Characters
{
    private $character;
    
    public function __construct(Character $character)
    {
        $this->character = $character;
    }

    /**
     * find
     *
     * @param int $characterId
     * 
     * @return mixed
     * 
     */
    public function find(int $characterId)
    {
        return $this->character->find($characterId);
    }

    /**
     * Updates a character when save is selected
     *
     * @param array $data
     * @param int $characterId
     *
     * @return bool
     */
    public function update(array $data, int $characterId)
    {
        return $this->character->update($data, $characterId);
    }

}