<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$version = 'v1';

$app->get('/', function () use ($app) {
    return 'Tasks';
});

$app->post('/character', $version.'\CharacterController@create');
$app->get('/character/{id:[0-9]{1,8}}', $version.'\CharacterController@fetch');
$app->get('/characters', $version.'\CharacterController@fetchAll');

$app->get('/start/{characterId:[0-9]{1,8}}', $version.'\ExploreController@start');
$app->get('/next-move/{characterId:[0-9]{1,8}}/{action:[0-3]{1}}', $version.'\ExploreController@nextMove');
$app->get('/battle/{characterId:[0-9]{1,8}}/{action:[1-2]{1}}', $version.'\ExploreController@battle');
// Saves the new gameplay
$app->patch('/next-move/{characterId:[0-9]{1,8}}/{action:[4]{1}}', $version.'\ExploreController@nextMove');
